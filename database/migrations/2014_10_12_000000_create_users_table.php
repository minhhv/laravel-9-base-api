<?php

use App\Enums\Gender;
use App\Enums\Role;
use App\Enums\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('role_id')->default(Role::USER);
            $table->string('name');
            $table->string('user_name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('confirm_code')->nullable();
            $table->enum('is_active', [
                Status::ACTIVE,
                Status::INACTIVE,
            ])->default(Status::INACTIVE);
            $table->integer('error_count')->default(0);
            $table->enum('is_locked', [
                Status::LOCK,
                Status::UNLOCK,
            ])->default(Status::UNLOCK);
            $table->bigInteger('point')->default(0);
            $table->string('avatar')->nullable();
            $table->enum('gender', [
                Gender::MALE,
                Gender::FEMALE,
                Gender::OTHER,
            ])->comment('M: MALE | F: FEMALE | O: OTHER')->nullable();
            $table->string('name_kana')->nullable();
            $table->string('address')->nullable();
            $table->timestamp('unlock_time')->nullable();
            $table->unsignedBigInteger('unlock_by')->nullable();
            $table->dateTime('birthday')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
