<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Enums\Status;
use DateTime;
use DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (! DB::table('users')->exists()) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('users')->truncate();
            $this->create();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    public function create(): void
    {
        $data = [
            [
                'role_id' => Role::ROOT_ADMIN,
                'user_name' => 'CuongPNV',
                'name' => 'CuongPNV',
                'email' => 'cuongpnv@bap.jp',
                'password' => Hash::make('!Vietcuong1234'),
                'confirm_code' => Str::random(32),
                'is_active' => Status::ACTIVE,
                'error_count' => '0',
                'is_locked' => Status::UNLOCK,
                'point' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'role_id' => Role::ROOT_ADMIN,
                'user_name' => 'user_name_baptest',
                'name' => 'baptest',
                'email' => 'user_baptest@yopmail.com',
                'password' => Hash::make('baptest123'),
                'confirm_code' => Str::random(32),
                'is_active' => Status::ACTIVE,
                'error_count' => '0',
                'is_locked' => Status::UNLOCK,
                'point' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'role_id' => Role::ADMIN,
                'user_name' => 'user_name_baptest_1',
                'name' => 'baptest_1',
                'email' => 'user_baptest_1@yopmail.com',
                'password' => Hash::make('baptest123'),
                'confirm_code' => Str::random(32),
                'is_active' => Status::ACTIVE,
                'error_count' => '0',
                'is_locked' => Status::UNLOCK,
                'point' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'role_id' => Role::USER,
                'user_name' => 'user_name_baptest_2',
                'name' => 'baptest_2',
                'email' => 'user_baptest_2@yopmail.com',
                'password' => Hash::make('baptest123'),
                'confirm_code' => Str::random(32),
                'is_active' => Status::ACTIVE,
                'error_count' => '0',
                'is_locked' => Status::UNLOCK,
                'point' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
        ];
        DB::table('users')->insert($data);
    }
}
