<?php

namespace Database\Seeders;

use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (! DB::table('permissions')->exists()) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('permissions')->truncate();
            $this->create();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    public function create(): void
    {
        $data = [
            [
                'name' => 'permission 1',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'name' => 'permission 2',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'name' => 'permission 3',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'name' => 'permission 4',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'name' => 'permission 5',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
        ];
        DB::table('permissions')->insert($data);
    }
}
