<?php

namespace Database\Seeders;

use App\Enums\Role;
use DateTime;
use DB;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (! DB::table('roles')->exists()) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('roles')->truncate();
            $this->create();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    public function create(): void
    {
        $data = [
            [
                'id' => Role::USER,
                'name' => 'USER',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'id' => Role::ADMIN,
                'name' => 'ADMIN',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'id' => Role::ROOT_ADMIN,
                'name' => 'ROOT_ADMIN',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'id' => Role::GUEST,
                'name' => 'GUEST',
                'guard_name' => 'api',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
        ];
        DB::table('roles')->insert($data);
    }
}
