<?php

namespace Database\Seeders;

use App\Enums\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (! DB::table('user_permissions')->exists()) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('user_permissions')->truncate();
            $this->create();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    public function create(): void
    {
        $data = [
            [
                'permission_id' => 1,
                'user_id' => 1,
                'status' => Status::ENABLED,
            ],
            [
                'permission_id' => 2,
                'user_id' => 1,
                'status' => Status::DISABLED,
            ],
        ];
        DB::table('user_permissions')->insert($data);
    }
}
