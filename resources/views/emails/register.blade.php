<!DOCTYPE html>
<html lang="">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    @media screen and (min-width: 768px) {
      body {
        max-width: 600px;
        margin: 0 auto;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        color: #000000;
      }
    }

    @media screen and (max-width: 767px) {
      body {
        padding: 10px;
        font-family: 'Roboto', sans-serif;
      }
    }
  </style>
  <title></title>
</head>

<body style="text-align: left;">
  <p>
    <em>{{ $user->email }}</em>様<br>
    Demoへのメールアドレス認証のご連絡です。<br>
    下記の認証用URLにアクセスいただくと、メールアドレスの認証が完了となります。
  </p>
  <p>
    <a style="text-decoration: underline; color: #0047FF;"
       href="{{ env('APP_FE_URL') }}/email-verification/{{ $confirmCode }}">認証用URLがここに入ります</a>
  </p>
  <p>
    ※URLの有効期限は14日です。期限内にアクセスしてください。<br>
    ※このメールにお心当たりがない場合は、第三者が誤ってあなたのメールアドレスを入力した可能性があります。その場合は、お手数ですが当メールの破棄をお願いいたします。
  </p>
  <p>
    また、本メールアドレスは返信を受け付けておりません。<br>
    お問い合わせは<a style="text-decoration: underline; color: #0047FF;"
                href="{{ env('EMAIL_ADMIN') }}">お問い合わせ先URL/メールアドレス</a>よりお願いいたします。
  </p>
  <p>Demoサポートデスク</p>
</body>
</html>