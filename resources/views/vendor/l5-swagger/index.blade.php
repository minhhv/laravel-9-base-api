<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{ l5_swagger_asset($documentation, 'swagger-ui.css') }}">
    <script src="{{ l5_swagger_asset($documentation, 'swagger-ui-bundle.js') }}"></script>
    <script src="{{ l5_swagger_asset($documentation, 'swagger-ui-standalone-preset.js') }}"></script>
    <style>
        .api-version-select {
            display: flex;
            align-items: center;
        }

        .api-version-select label {
            margin-right: 1rem;
        }

        .api-version-select select {
            padding: 0.5rem;
            border-radius: 0.25rem;
            margin-right: 1rem;
            border: 1px solid #ccc;
            font-size: 1rem;
        }

        .api-version-select button {
            padding: 0.5rem 1rem;
            border-radius: 0.25rem;
            background-color: #007bff;
            color: #fff;
            border: none;
            cursor: pointer;
            font-size: 1rem;
        }

        .api-version-select button:hover {
            background-color: #0069d9;
        }

        .api-version-container {
            float: right;
            margin-right: 20px;
            margin-top: 10px;
        }

        label[for="api-version"] {
            color: white;
        }

        .servers,
        .servers-title {
            display: none;
        }
    </style>
</head>

<body>
    <div class="api-version-select api-version-container">
        <label for="api-version">Choose an API version:</label>
        <select id="api-version" name="api-version" onchange="loadSwagger()">
            <option value="v1" selected>API V1</option>
        </select>
    </div>
    <div id="swagger-ui">
    </div>

    <script>
        function loadSwagger() {
            const apiVersion = document.getElementById("api-version").value
            localStorage.setItem("api-version", apiVersion)
            const swaggerUrl = "{{ url('/docs') }}/" + apiVersion + "/api-docs.json"
            swaggerUI = SwaggerUIBundle({
                url: swaggerUrl,
                dom_id: '#swagger-ui',
                deepLinking: true,
                presets: [
                    SwaggerUIBundle.presets.apis,
                    SwaggerUIStandalonePreset
                ],
                plugins: [
                    SwaggerUIBundle.plugins.DownloadUrl
                ],
                persistAuthorization: true,
                layout: "StandaloneLayout",
                // docExpansion: 'none',
                requestInterceptor: function(request) {
                    request.headers['X-CSRF-TOKEN'] = '{{ csrf_token() }}';
                    return request;
                }
            })
        }

        document.addEventListener("DOMContentLoaded", function() {
            var savedApiVersion = localStorage.getItem("api-version");
            if (savedApiVersion === null) {
                savedApiVersion = document.getElementById("api-version").value
            }
            document.getElementById("api-version").value = savedApiVersion;
            loadSwagger();
        });
    </script>
</body>

</html>