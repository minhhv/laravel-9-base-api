<?php

use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(static function (): void {
    Route::post('/register', [UserController::class, 'register']);
    Route::post('/email-verify/{confirm_code}', [UserController::class, 'verifyRegistration']);
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/reset-password', [LoginController::class, 'resetPassword']);
    Route::post('/reset-password/{confirm_code}', [LoginController::class, 'updatePassword']);
});

Route::middleware('auth:api')->group(static function (): void {
    Route::prefix('auth')->group(static function (): void {
        Route::post('/logout', [LoginController::class, 'logout']);
    });

    Route::prefix('me')->group(static function (): void {
        Route::get('/profile', [UserController::class, 'getProfile']);
        Route::get('/role', [UserController::class, 'roles']);
        Route::get('/permission', [UserController::class, 'getPermissions']);
        Route::post('/change-password', [UserController::class, 'changePassword']);
    });
});
