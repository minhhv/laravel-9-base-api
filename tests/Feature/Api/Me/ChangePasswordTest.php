<?php

namespace Tests\Feature\Api\Me;

use App\Services\Common\LogDebugService;
use Illuminate\Support\Facades\DB;
use Tests\Feature\Traits\FakeAuthTrait;
use Tests\TestCase;
use Throwable;

class ChangePasswordTest extends TestCase
{
    use FakeAuthTrait;

    private string $url;

    public function __construct()
    {
        parent::__construct();

        $this->url = '/api/v1/me/change-password';
    }

    /**
     * Test change password success
     *
     * @return void
     * @throws Throwable
     */
    public function testChangePasswordSuccess()
    {
        DB::beginTransaction();

        try {
            $dataUserTest = $this->createUserTest();
            $user = $dataUserTest['user'];
            $ip = $dataUserTest['ip'];

            $responseLogin = $this->loginTest($user->email);

            $dataInput = [
                'old_password' => 'baptest123',
                'new_password' => 'baptest12345',
                'new_password_confirmation' => 'baptest12345',
            ];

            $response = $this->postJson(
                $this->url,
                $dataInput,
                ['Authorization' => "Bearer {$responseLogin['data']['access_token']}"]
            );

            $response->assertStatus(200);

            $this->deleteTestUser($user->id, $ip->id);

            DB::commit();
        } catch (Throwable $e) {
            LogDebugService::debugExceptionMessage($e);

            DB::rollBack();
        }
    }

    /**
     * Test change password validate fail
     *
     * @return void
     * @throws Throwable
     */
    public function testChangePasswordValidateFail()
    {
        DB::beginTransaction();

        try {
            $dataUserTest = $this->createUserTest();
            $user = $dataUserTest['user'];
            $ip = $dataUserTest['ip'];

            $responseLogin = $this->loginTest($user->email);

            $dataInput = [
                'old_password' => 'baptest123',
                'new_password' => 'baptest', // not longer than min length
                'new_password_confirmation' => 'baptest12345',
            ];

            $response = $this->postJson(
                $this->url,
                $dataInput,
                ['Authorization' => "Bearer {$responseLogin['data']['access_token']}"]
            );

            $response->assertStatus(422);

            $this->deleteTestUser($user->id, $ip->id);

            DB::commit();
        } catch (Throwable $e) {
            LogDebugService::debugExceptionMessage($e);

            DB::rollBack();
        }
    }

    /**
     * Test change password not confirmed fail
     *
     * @return void
     * @throws Throwable
     */
    public function testChangePasswordNotConfirmedFail()
    {
        DB::beginTransaction();

        try {
            $dataUserTest = $this->createUserTest();
            $user = $dataUserTest['user'];
            $ip = $dataUserTest['ip'];

            $responseLogin = $this->loginTest($user->email);

            $dataInput = [
                'old_password' => 'baptest123',
                'new_password' => 'baptest12345',
                'new_password_confirmation' => 'baptest123456', // not confirmed new password
            ];

            $response = $this->postJson(
                $this->url,
                $dataInput,
                ['Authorization' => "Bearer {$responseLogin['data']['access_token']}"]
            );

            $response->assertStatus(422);

            $this->deleteTestUser($user->id, $ip->id);

            DB::commit();
        } catch (Throwable $e) {
            LogDebugService::debugExceptionMessage($e);

            DB::rollBack();
        }
    }

    /**
     * Test change password old password not true fail
     *
     * @return void
     * @throws Throwable
     */
    public function testChangePasswordOldPasswordNotTrueFail()
    {
        DB::beginTransaction();

        try {
            $dataUserTest = $this->createUserTest();
            $user = $dataUserTest['user'];
            $ip = $dataUserTest['ip'];

            $responseLogin = $this->loginTest($user->email);

            $dataInput = [
                'old_password' => 'baptest123333', // not true
                'new_password' => 'baptest12345',
                'new_password_confirmation' => 'baptest12345',
            ];

            $response = $this->postJson(
                $this->url,
                $dataInput,
                ['Authorization' => "Bearer {$responseLogin['data']['access_token']}"]
            );

            $response->assertStatus(401);

            $this->deleteTestUser($user->id, $ip->id);

            DB::commit();
        } catch (Throwable $e) {
            LogDebugService::debugExceptionMessage($e);

            DB::rollBack();
        }
    }

    /**
     * Test change authentication fail
     *
     * @return void
     */
    public function testChangePasswordAuthenticationFail()
    {
        $dataInput = [
            'old_password' => 'baptest123',
            'new_password' => 'baptest12345',
            'new_password_confirmation' => 'baptest12345',
        ];

        $response = $this->postJson(
            $this->url,
            $dataInput
        );

        $response->assertStatus(401);
    }
}
