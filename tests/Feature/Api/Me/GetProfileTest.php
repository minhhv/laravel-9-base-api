<?php

namespace Tests\Feature\Api\Me;

use App\Services\Common\LogDebugService;
use Illuminate\Support\Facades\DB;
use Tests\Feature\Traits\FakeAuthTrait;
use Tests\TestCase;
use Throwable;

class GetProfileTest extends TestCase
{
    use FakeAuthTrait;

    /**
     * Test get profile success
     *
     * @return void
     * @throws Throwable
     */
    public function testGetProfileSuccess()
    {
        DB::beginTransaction();

        try {
            $dataUserTest = $this->createUserTest();
            $user = $dataUserTest['user'];
            $ip = $dataUserTest['ip'];

            $responseLogin = $this->loginTest($user->email);

            $response = $this->json(
                'GET',
                '/api/v1/me/profile',
                [],
                ['Authorization' => "Bearer {$responseLogin['data']['access_token']}"]
            );

            $response->assertStatus(200);

            $this->deleteTestUser($user->id, $ip->id);

            DB::commit();
        } catch (Throwable $e) {
            LogDebugService::debugExceptionMessage($e);

            DB::rollBack();
        }
    }

    /**
     * Test get profile fail (not login)
     *
     * @return void
     */
    public function testGetProfileFail()
    {
        $response = $this->json(
            'GET',
            '/api/v1/me/profile',
        );

        $response->assertStatus(401);
    }
}
