<?php

namespace Tests\Feature\Api\Auth;

use App\Enums\Role;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Str;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testResetPasswordSuccess()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_resetPass@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);

        $dataLogin = [
            'email' => $user->email,
            'password' => 'baptest123',
        ];
        $responseLogin = $this->post('/api/v1/auth/login', $dataLogin);

        $token = PasswordReset::create([
            'email' => $user->email,
            'token' => Str::random(32),
        ]);
        $data = [
            'password' => 'baptest123',
            'password_confirmation' => 'baptest123',
        ];
        $response = $this->postJson(
            '/api/v1/auth/reset-password/' . $token->token,
            $data,
            [
                'Authorization' => "Bearer {$responseLogin['data']['access_token']}",
            ]
        );

        $response->assertStatus(200);

        User::where('id', $user->id)->forceDelete();
        PasswordReset::where('email', $user->email)->forceDelete();
    }

    public function testResetPasswordFail()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_resetPass@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);

        $dataLogin = [
            'email' => $user->email,
            'password' => 'baptest123',
        ];
        $responseLogin = $this->post('/api/v1/auth/login', $dataLogin);

        PasswordReset::create([
            'email' => $user->email,
            'token' => Str::random(32),
        ]);

        $data = [
            'password' => 'baptest123',
            'password_confirmation' => 'baptest123',
        ];
        $response = $this->postJson(
            '/api/v1/auth/reset-password/abcd',
            $data,
            [
                'Authorization' => "Bearer {$responseLogin['data']['access_token']}",
            ]
        );

        $response->assertStatus(500);

        User::where('id', $user->id)->forceDelete();
        PasswordReset::where('email', $user->email)->forceDelete();
    }

    public function testResetPasswordValidator()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_resetPass@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);

        $dataLogin = [
            'email' => $user->email,
            'password' => 'baptest123',
        ];
        $responseLogin = $this->post('/api/v1/auth/login', $dataLogin);

        $token = PasswordReset::create([
            'email' => $user->email,
            'token' => Str::random(32),
        ]);
        $data = [
            'password' => 'baptest123',
            'password_confirmation' => 'baptest12345',
        ];
        $response = $this->postJson(
            '/api/v1/auth/reset-password/' . $token->token,
            $data,
            [
                'Authorization' => "Bearer {$responseLogin['data']['access_token']}",
            ]
        );

        $response->assertStatus(422);

        User::where('id', $user->id)->forceDelete();
        PasswordReset::where('email', $user->email)->forceDelete();
    }
}
