<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegisterSuccess()
    {
        $data = [
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_register@yopmail.com',
            'password' => 'baptest123',
            'password_confirmation' => 'baptest123',
        ];

        $response = $this->postJson(
            '/api/v1/user/register',
            $data
        );

        $response->assertStatus(200);

        User::where('email', 'user_baptest_register@yopmail.com')->forceDelete();
    }

    public function testRegisterValidator()
    {
        $data = [
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_register@yopmail.com',
            'password' => 'baptest123',
            'password_confirmation' => 'baptest123456',
        ];

        $response = $this->postJson(
            '/api/v1/user/register',
            $data
        );

        $response->assertStatus(422);

        User::where('email', 'user_baptest_register@yopmail.com')->forceDelete();
    }
}
