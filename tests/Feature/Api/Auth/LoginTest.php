<?php

namespace Tests\Feature\Api\Auth;

use App\Enums\Role;
use App\Models\IpAccess;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Request;
use Str;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLoginSuccess()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_login@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);
        $dataLogin = [
            'email' => $user->email,
            'password' => 'baptest123',
        ];
        $response = $this->post('/api/v1/auth/login', $dataLogin);
        $response->assertStatus(200);

        User::where('id', $user->id)->forceDelete();
    }

    public function testLoginFailPassword()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_login@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);
        $dataLogin = [
            'email' => $user->email,
            'password' => 'test123456',
        ];
        $response = $this->post('/api/v1/auth/login', $dataLogin);
        $response->assertStatus(422);

        User::where('id', $user->id)->forceDelete();
    }

    public function testLoginFail()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_login@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);
        $dataLogin = [
            'email' => $user->email,
            'password' => 'testbap123',
        ];
        $response = $this->post('/api/v1/auth/login', $dataLogin);
        $response->assertStatus(422);

        User::where('id', $user->id)->forceDelete();
    }

    public function testLoginFailValidator()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_login@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);
        $dataLogin = [
            'email' => '',
            'password' => 'baptest123',
        ];
        $response = $this->post('/api/v1/auth/login', $dataLogin);
        $response->assertStatus(422);

        User::where('id', $user->id)->forceDelete();
    }

    public function testLoginFailLocked()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_login@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '1',
            'point' => 0,
        ]);
        $dataLogin = [
            'email' => $user->email,
            'password' => 'baptest123',
        ];

        $response = $this->post('/api/v1/auth/login', $dataLogin);
        $response->assertStatus(422);

        User::where('id', $user->id)->forceDelete();
    }

    public function testLoginFailIpAccess()
    {
        $user = User::create([
            'role_id' => Role::USER,
            'user_name' => 'user_name_baptest',
            'name' => 'baptest',
            'email' => 'user_baptest_login@yopmail.com',
            'password' => Hash::make('baptest123'),
            'confirm_code' => Str::random(32),
            'is_active' => '1',
            'error_count' => '0',
            'is_locked' => '0',
            'point' => 0,
        ]);
        $dataLogin = [
            'email' => $user->email,
            'password' => 'baptest123',
        ];
        $request = Request::create('/api/v1/auth/login', 'post');
        $ip = $request->ip();
        IpAccess::where('ip_address', $ip)->update(['ip_address' => '0.0.0.0']);

        $response = $this->post('/api/v1/auth/login', $dataLogin);
        $response->assertStatus(422);

        User::where('id', $user->id)->forceDelete();
        IpAccess::where('ip_address', '0.0.0.0')->update(['ip_address' => $ip]);
    }
}
