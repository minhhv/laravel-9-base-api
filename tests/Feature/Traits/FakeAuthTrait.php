<?php

namespace Tests\Feature\Traits;

use App\Enums\Gender;
use App\Enums\Role;
use App\Enums\Status;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Testing\TestResponse;

trait FakeAuthTrait
{
    /**
     * Post login test
     *
     * @param $email
     * @return TestResponse
     */
    public function loginTest($email): TestResponse
    {
        $dataLogin = [
            'email' => $email,
            'password' => 'baptest123',
        ];

        return $this->post('/api/v1/auth/login', $dataLogin);
    }

    /**
     * Create user test
     *
     * @return array
     */
    public function createUserTest(): array
    {
        $user = User::create(
            [
                'role_id' => Role::USER,
                'name' => Str::random(10),
                'user_name' => Str::random(10),
                'email' => 'baptest_' . Str::random(10) . '@yopmail.com',
                'password' => Hash::make('baptest123'),
                'confirm_code' => Str::random(32),
                'is_active' => Status::ACTIVE,
                'error_count' => 0,
                'is_locked' => Status::UNLOCK,
                'point' => 10,
                'gender' => Gender::MALE,
            ]
        );

        return [
            'user' => $user,
        ];
    }
}
