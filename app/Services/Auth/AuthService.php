<?php

namespace App\Services\Auth;

use App\Enums\ErrorType;
use App\Enums\SiteConfig;
use App\Enums\Status;
use App\Jobs\SendPasswordResetEmailJob;
use App\Repositories\PasswordResetRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\PendingDispatch;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class AuthService
{
    protected UserRepositoryInterface $userRepository;

    protected PasswordResetRepositoryInterface $passwordResetRepositoryInterface;

    public function __construct(
        UserRepositoryInterface $userRepository,
        PasswordResetRepositoryInterface $passwordResetRepositoryInterface
    ) {
        $this->userRepository = $userRepository;
        $this->passwordResetRepositoryInterface = $passwordResetRepositoryInterface;
    }

    /**
     * Login
     *
     * @param $data
     * @return array
     * @throws ValidationException
     */
    public function login($data): array
    {
        $user = $this->userRepository->findByEmail($data->email);

        if (! $user || $user->is_locked === Status::LOCK) {
            $errorType = ! $user ? ErrorType::ERROR_EMAIL : ErrorType::ERROR_USER_LOCK;

            return $this->returnError($errorType);
        }

        if (! Hash::check($data->password, $user->password)) {
            $remainingLoginAttempts = $this->updateLock($user);

            return $this->returnError(ErrorType::ERROR_PASSWORD, $remainingLoginAttempts);
        }

        if ($user->is_active === Status::ACTIVE) {
            $user->error_count = SiteConfig::ERROR_RESET;
            $user->is_locked = Status::UNLOCK;
            $user->save();

            $tokenResult = $user->createToken('Personal Access Token');

            return [
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
                'user'  => $user,
                'isAuthenticate'  => true,
            ];
        }

        throw ValidationException::withMessages(['error' => __('message.account_not_active')]);
    }

    /**
     * Return error
     *
     * @param $error
     * @param $remainingLoginAttempts
     * @return array
     */
    private function returnError($error, $remainingLoginAttempts = null): array
    {
        return [
            'error' => $error,
            'isAuthenticate' => false,
            'remaining_login_attempts' => $remainingLoginAttempts,
        ];
    }

    /**
     * Update lock
     *
     * @param $user
     * @return int
     */
    private function updateLock($user): int
    {
        $user->error_count++;
        $maxAllowedErrors = SiteConfig::ERROR_COUNT;

        if ($user->error_count >= $maxAllowedErrors) {
            $user->is_locked = Status::LOCK;
        }

        $user->save();

        return SiteConfig::ERROR_COUNT - $user->error_count;
    }

    /**
     * Find by email
     *
     * @param $request
     * @return mixed
     */
    public function findByEmail($request): mixed
    {
        return $this->userRepository->findByEmail($request->email);
    }

    /**
     * Find token by email
     *
     * @param $email
     * @return mixed
     */
    public function findTokenByEmail($email): mixed
    {
        return $this->passwordResetRepositoryInterface->findTokenByEmail($email);
    }

    /**
     * Update token by email
     *
     * @param $email
     * @param $confirmCode
     * @return mixed
     */
    public function updateTokenByEmail($email, $confirmCode): mixed
    {
        return $this->passwordResetRepositoryInterface->updateTokenByEmail($email, $confirmCode);
    }

    /**
     * Create reset token
     *
     * @param $email
     * @param $confirmCode
     * @return mixed
     */
    public function createResetToken($email, $confirmCode): mixed
    {
        return $this->passwordResetRepositoryInterface->createResetToken($email, $confirmCode);
    }

    /**
     * Reset confirm mail
     *
     * @param $user
     * @param $confirmCode
     * @return PendingDispatch
     */
    public function resetConfirmMail($user, $confirmCode): PendingDispatch
    {
        return SendPasswordResetEmailJob::dispatch($user, $confirmCode);
    }

    /**
     * Find by confirm code
     *
     * @param $confirmCode
     * @return mixed
     */
    public function findByConfirmCode($confirmCode): mixed
    {
        return $this->passwordResetRepositoryInterface->findByConfirmCode($confirmCode);
    }

    /**
     * Update password reset
     *
     * @param string $email
     * @param string $password
     * @return void
     */
    public function updatePasswordReset(string $email, string $password): void
    {
        $this->userRepository->updatePasswordReset($email, $password);
    }

    /**
     * Delete reset token
     *
     * @param string $email
     * @return void
     */
    public function deleteResetToken(string $email): void
    {
        $this->passwordResetRepositoryInterface->deleteResetToken($email);
    }

    /**
     * Reset password
     *
     * @param $request
     * @return array
     */
    public function resetPassword($request): array
    {
        $user = $this->findByEmail($request);

        if (! $user) {
            return $this->returnErrorReset(Response::HTTP_UNPROCESSABLE_ENTITY, __('message.account_not_exist'));
        }

        if ($user->is_locked === Status::LOCK) {
            return $this->returnErrorReset(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('message.invalid_email_address_not_found')
            );
        }

        if ($user->is_active === Status::INACTIVE) {
            return $this->returnErrorReset(Response::HTTP_UNPROCESSABLE_ENTITY, __('message.not_verified'));
        }

        $confirmCode = Str::random(32);
        $tokenExists = $this->findTokenByEmail($request->email);

        if ($tokenExists) {
            $this->updateTokenByEmail($request->email, $confirmCode);
        } else {
            $this->createResetToken($request->email, $confirmCode);
        }

        $this->resetConfirmMail($user, $confirmCode);

        return [
            'status' => Response::HTTP_OK,
            'message' => __('message.email_sent_password_reset'),
        ];
    }

    /**
     * Return error reset
     *
     * @param $status
     * @param $message
     * @return array
     */
    private function returnErrorReset($status, $message): array
    {
        return [
            'status' => $status,
            'message' =>  $message,
        ];
    }
}
