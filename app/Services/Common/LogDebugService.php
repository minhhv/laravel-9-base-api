<?php

namespace App\Services\Common;

use Illuminate\Support\Facades\Log;
use Throwable;

class LogDebugService
{
    /**
     * Common log debug exception massage
     */
    public static function debugExceptionMessage(Throwable $e): void
    {
        Log::debug(now() . ' > ' . $e->getMessage() . ' > ' . request()->url());
    }
}
