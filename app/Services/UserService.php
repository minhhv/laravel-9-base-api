<?php

namespace App\Services;

use App\Jobs\SendConfirmationEmailJob;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Foundation\Bus\PendingDispatch;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserService
{
    protected UserRepositoryInterface $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Register user
     *
     * @param $request
     * @return User
     */
    public function register($request): User
    {
        $existUser = $this->userRepository->findByEmail($request->email);
        $confirmCode = Str::random(32);

        if ($existUser && $existUser->is_active === 0) {
            $this->userRepository->updateUserConfirmCodeByEmail($request->email, $confirmCode);
        }

        $userData = [
            'user_name' => $request->input('user_name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'confirm_code' => $confirmCode,
        ];

        return $this->userRepository->createUser($userData);
    }

    /**
     * Send confirmation mail
     *
     * @param $existUser
     * @param $confirmCode
     * @return PendingDispatch
     */
    public function sendConfirmationEmail($existUser, $confirmCode): PendingDispatch
    {
        return SendConfirmationEmailJob::dispatch($existUser, $confirmCode);
    }

    /**
     * @param $confirmCode
     * @return mixed
     */
    public function verifyRegistration($confirmCode): mixed
    {
        return User::where('confirm_code', $confirmCode)->first();
    }

    /**
     * Get roles
     *
     * @param $request
     * @return mixed
     */
    public function roles($request): mixed
    {
        return $this->userRepository->roles($request);
    }

    /**
     * Get permissions
     *
     * @param $request
     * @return mixed
     */
    public function getPermissions($request): mixed
    {
        return $this->userRepository->getPermissions($request);
    }

    /**
     * Get list user
     *
     * @param $request
     * @return mixed
     */
    public function users($request): mixed
    {
        $data = [
            'per_page' => $request->per_page ?? config('const.per_page_user'),
        ];

        return $this->userRepository->users($data);
    }

    /**
     * Get profile
     *
     * @param $request
     * @return mixed
     */
    public function getProfile($request): mixed
    {
        return $this->userRepository->getProfile($request->user()->id);
    }

    /**
     * Change password
     *
     * @param $request
     * @return mixed
     */
    public function changePassword($request): mixed
    {
        $user = $request->user();
        $user->password = Hash::make($request->new_password);
        $user->save();

        return $user;
    }
}
