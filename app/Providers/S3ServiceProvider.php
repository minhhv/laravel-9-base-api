<?php

namespace App\Providers;

use Aws\S3\S3Client;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

class S3ServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('s3filesystem', static function () {
            $s3Config = [
                'credentials' => [
                    'key' => env('AWS_ACCESS_KEY_ID'),
                    'secret' => env('AWS_SECRET_ACCESS_KEY'),
                ],
                'region' => env('AWS_DEFAULT_REGION'),
                'version' => 'latest',
            ];

            $s3Client = new S3Client($s3Config);

            $adapter = new AwsS3Adapter($s3Client, env('AWS_BUCKET'));

            return new Filesystem($adapter);
        });
    }
}
