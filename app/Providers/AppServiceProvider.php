<?php

namespace App\Providers;

use App\Repositories\Impl\UserRepositoryImpl;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //check that app is local
        if ($this->app->isLocal()) {
        //if local register your services you require for development
            URL::forceScheme('http');
        } else {
        //else register your services you require for production
            URL::forceScheme('https');
        }

        $this->app->singleton(
            UserRepositoryInterface::class,
            UserRepositoryImpl::class
        );
    }
}
