<?php

namespace App\Http\Middleware;

use App\Enums\Role;
use App\Http\Component\ResponseComponent;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VerifyRoleAdmin
{
    protected ResponseComponent $responseComponent;

    public function __construct(
        ResponseComponent $responseComponent
    ) {
        $this->responseComponent = $responseComponent;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response|RedirectResponse) $next
     * @return Response|RedirectResponse|JsonResponse
     */
    public function handle(Request $request, Closure $next): Response|RedirectResponse|JsonResponse
    {
        if (
            $request->user()
            && (
                $request->user()->role_id === Role::ROOT_ADMIN
                || $request->user()->role_id === Role::ADMIN
            )
        ) {
            return $next($request);
        }

        return $this->responseComponent->unauthorized();
    }
}
