<?php

namespace App\Http\Middleware;

use App\Traits\APIResponse;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Authenticate extends Middleware
{
    use APIResponse;

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     */
    protected function redirectTo($request): mixed
    {
        if (! $request->expectsJson()) {
            return $this->responseError(__('error.unexpected_json'), Response::HTTP_BAD_REQUEST);
        }

        return $this->responseError(__('error.bad_request'), Response::HTTP_BAD_REQUEST);
    }
}
