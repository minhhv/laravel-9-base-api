<?php

namespace App\Http\Controllers\Api;

use App\Enums\Expire;
use App\Enums\Status;
use App\Http\Component\ResponseComponent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\UserRegistrationRequest;
use App\Http\Resources\UserDataResource;
use App\Services\Common\LogDebugService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class UserController extends Controller
{
    protected UserService $userService;

    protected ResponseComponent $responseComponent;

    public function __construct(
        UserService $userService,
        ResponseComponent $responseComponent
    ) {
        $this->userService = $userService;
        $this->responseComponent = $responseComponent;
    }

    /**
     * Register a new user with the provided registration request.
     *
     * @param  UserRegistrationRequest  $request
     * @return Response
     */
    public function register(UserRegistrationRequest $request): Response
    {
        $user = $this->userService->register($request);
        $confirmCode = $user->confirm_code;
        $this->sendConfirmationEmail($user, $confirmCode);

        return $this->responseComponent->success($user);
    }

    /**
     * Send a confirmation email to the existing user with the provided confirmation code.
     *
     * @param  $existUser
     * @param  $confirmCode
     * @return Response
     */
    public function sendConfirmationEmail($existUser, $confirmCode): Response
    {
        try {
            $this->userService->sendConfirmationEmail($existUser, $confirmCode);

            return $this->responseComponent->customResponse(
                Response::HTTP_OK,
                __('message.check_mail')
            );
        } catch (Throwable $e) {
            LogDebugService::debugExceptionMessage($e);

            return $this->responseComponent->customResponse(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                __('message.error_send_mail')
            );
        }
    }

    /**
     * Verify user registration using the provided confirmation code.
     *
     * @param  $confirmCode
     * @return Response
     */
    public function verifyRegistration($confirmCode): Response
    {
        $user = $this->userService->verifyRegistration($confirmCode);

        if ($user) {
            if ($user->is_active === Status::ACTIVE) {
                return $this->responseComponent->customResponse(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    __('message.email_already_verified')
                );
            }

            $createdAt = Carbon::parse($user->created_at);
            $currentDate = Carbon::now();

            if ($createdAt->diffInDays($currentDate) <= Expire::DAY_EXPIRE) {
                $user->is_active = Status::ACTIVE;
                $user->save();

                return $this->responseComponent->success($user);
            }

            return $this->responseComponent->customResponse(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('message.expired_verified')
            );
        }

        return $this->responseComponent->customResponse(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            __('message.invalid_user')
        );
    }

    /**
     * Get the profile of the authenticated user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function getProfile(Request $request): Response
    {
        $result = $this->userService->getProfile($request);

        if (! $result) {
            return $this->responseComponent->internalServerError();
        }

        return $this->responseComponent->success(new UserDataResource($result));
    }

    /**
     * Get role of the authenticated user.
     *
     * @param Request $request The HTTP request object.
     * @return Response
     */
    public function roles(Request $request): Response
    {
        $result = $this->userService->roles($request);

        if (! $result) {
            return $this->responseComponent->internalServerError();
        }

        return $this->responseComponent->success($result);
    }

    /**
     * Get permission of the authenticated user.
     *
     * @param Request $request The HTTP request object.
     * @return Response
     */
    public function getPermissions(Request $request): Response
    {
        $result = $this->userService->getPermissions($request);

        if (! $result) {
            return $this->responseComponent->internalServerError();
        }

        return $this->responseComponent->success($result);
    }

    /**
     * Get all user role admin.
     *
     * @param Request $request
     * @return Response
     */
    public function users(Request $request): Response
    {
        $users = $this->userService->users($request);

        return $this->responseComponent->success($users);
    }

    /**
     * User change password
     *
     * @param ChangePasswordRequest $request
     * @return Response
     */
    public function changePassword(ChangePasswordRequest $request): Response
    {
        $user = $request->user();

        if (! Hash::check($request->old_password, $user->password)) {
            return  $this->responseComponent->customResponse(
                Response::HTTP_UNAUTHORIZED,
                __('message.old_password_is_incorrect')
            );
        }

        $result = $this->userService->changePassword($request);

        return $this->responseComponent->success($result);
    }
}
