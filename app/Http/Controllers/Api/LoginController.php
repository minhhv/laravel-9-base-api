<?php

namespace App\Http\Controllers\Api;

use App\Enums\Expire;
use App\Http\Component\ResponseComponent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use App\Services\Auth\AuthService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    protected AuthService $authService;

    protected ResponseComponent $responseComponent;

    public function __construct(
        AuthService $authService,
        ResponseComponent $responseComponent
    ) {
        $this->authService = $authService;
        $this->responseComponent = $responseComponent;
    }

    /**
     * Login
     *
     * @param LoginRequest $request
     * @return Response
     * @throws ValidationException
     */
    public function login(LoginRequest $request): Response
    {
        $authenticate = $this->authService->login($request);

        if (! $authenticate['isAuthenticate']) {
            $dataError = [
                'error_type' => $authenticate['error'],
            ];

            if ($authenticate['remaining_login_attempts']) {
                $dataError['remaining_login_attempts'] = $authenticate['remaining_login_attempts'];
            }

            return $this->responseComponent->validationErrorWithData(
                $dataError,
                __('auth.login_fail')
            );
        }

        return $this->responseComponent->success($authenticate);
    }

    /**
     * Reset password
     *
     * @param ResetPasswordRequest $request
     * @return Response
     */
    public function resetPassword(ResetPasswordRequest $request): Response
    {
        $result = $this->authService->resetPassword($request);

        return $this->responseComponent->customResponse($result['status'], $result['message']);
    }

    /**
     * Update password
     *
     * @param $token
     * @param UpdatePasswordRequest $request
     * @return Response
     */
    public function updatePassword($token, UpdatePasswordRequest $request): Response
    {
        $user = $this->authService->findByConfirmCode($token, $request);

        if (! $user) {
            return $this->responseComponent->customResponse(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('message.url_exist_expired')
            );
        }

        $createdAt = Carbon::parse($user->created_at);
        $currentDate = Carbon::now();

        if ($createdAt->diffInDays($currentDate) <= Expire::DAY_EXPIRE) {
            $this->authService->updatePasswordReset($user->email, $request->password);
            $this->authService->deleteResetToken($user->email);

            return $this->responseComponent->customResponse(
                Response::HTTP_OK,
                __('message.success_update_password')
            );
        } else {
            return $this->responseComponent->customResponse(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('message.expired_verified')
            );
        }
    }

    /**
     * Logout
     *
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request): Response
    {
        $token = $request->user()->token();
        $token->revoke();

        return $this->responseComponent->customResponse(
            Response::HTTP_OK,
            __('message.logout_success')
        );
    }
}
