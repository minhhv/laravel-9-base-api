<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;
use App\Rules\Auth\StrongPasswordRule;

class UserRegistrationRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'user_name' => [
                'bail',
                'required',
                'string',
                'min:' . config('const.user_name_min_length'),
                'max:' . config('const.user_name_max_length'),
                'unique:App\Models\User,user_name,NULL,id,deleted_at,NULL',
            ],
            'email' => [
                'bail',
                'required',
                'regex:' . config('const.regex_email'),
                'min:' . config('const.email_register_min_length'),
                'max:' . config('const.email_register_max_length'),
                'unique:App\Models\User,email,NULL,id,deleted_at,NULL',
            ],
            'password' => [
                'bail',
                'required',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
                new StrongPasswordRule(),
                'confirmed',
            ],
            'password_confirmation' => [
                'bail',
                'required',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
            ],
        ];
    }
}
