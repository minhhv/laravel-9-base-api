<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;

class LoginRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'email' => [
                'bail',
                'required',
                'regex:' . config('const.regex_email'),
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
            ],
            'password' => [
                'bail',
                'required',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
            ],
        ];
    }
}
