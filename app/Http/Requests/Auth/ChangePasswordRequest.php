<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;

class ChangePasswordRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'old_password' => [
                'bail',
                'required',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
            ],
            'new_password' => [
                'bail',
                'required',
                'confirmed',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
            ],
            'new_password_confirmation' => [
                'bail',
                'required',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
            ],
        ];
    }
}
