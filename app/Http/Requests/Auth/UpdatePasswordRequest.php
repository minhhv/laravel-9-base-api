<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;
use App\Rules\Auth\StrongPasswordRule;

class UpdatePasswordRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'password' => [
                'bail',
                'required',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
                new StrongPasswordRule(),
                'confirmed',
            ],
            'password_confirmation' => [
                'bail',
                'required',
                'min:' . config('const.password_length'),
                'max:' . config('const.password_max_length'),
            ],
        ];
    }
}
