<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class UserDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request): array|Arrayable|JsonSerializable
    {
        return [
            'id' => $this->resource->id,
            'role_name' => $this->resource->role_name,
            'name' => $this->resource->name,
            'user_name' => $this->resource->user_name,
            'email' => $this->resource->email,
            'is_active' => $this->resource->is_active,
            'error_count' => $this->resource->error_count,
            'point' => $this->resource->point,
            'avatar' => $this->resource->avatar,
            'gender' => $this->resource->gender,
            'name_kana' => $this->resource->name_kana,
            'address' => $this->resource->address,
            'unlock_time' => $this->resource->unlock_time,
            'unlock_by' => $this->resource->unlock_by,
            'birthday' => $this->resource->birthday,
        ];
    }
}
