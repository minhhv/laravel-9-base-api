<?php

namespace App\Repositories;

use App\Models\User;

interface UserRepositoryInterface
{
    /**
     * Find by email
     *
     * @param $email
     * @return mixed
     */
    public function findByEmail($email): mixed;

    /**
     * Update user confirm code by email
     *
     * @param $email
     * @param $confirmCode
     * @return int|bool
     */
    public function updateUserConfirmCodeByEmail($email, $confirmCode): int|bool;

    /**
     * Store new user
     *
     * @param $userData
     * @return User
     */
    public function createUser($userData): User;

    /**
     * Get roles
     *
     * @param $request
     * @return mixed
     */
    public function roles($request): mixed;

    /**
     * Get permissions
     *
     * @param $request
     * @return mixed
     */
    public function getPermissions($request): mixed;

    /**
     * Get list user
     *
     * @param $data
     * @return mixed
     */
    public function users($data): mixed;

    /**
     * Get user with user role
     *
     * @param $id
     * @return mixed
     */
    public function getUserWithUserRole($id): mixed;

    /**
     * Update password reset
     *
     * @param $email
     * @param $password
     * @return void
     */
    public function updatePasswordReset($email, $password): void;

    /**
     * Get profile
     *
     * @param $userId
     * @return mixed
     */
    public function getProfile($userId): mixed;

    /**
     * Search users
     *
     * @param $request
     * @return mixed
     */
    public function search($request): mixed;

    /**
     * Find valid user by id
     *
     * @param $id
     * @return mixed
     */
    public function findValidUserById($id): mixed;
}
