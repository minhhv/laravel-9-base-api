<?php

namespace App\Repositories;

interface PasswordResetRepositoryInterface
{
    /**
     * Find token by email
     *
     * @param string $email
     * @return mixed
     */
    public function findTokenByEmail(string $email): mixed;

    /**
     * Update token by email
     *
     * @param string $email
     * @param string $confirmCode
     * @return mixed
     */
    public function updateTokenByEmail(string $email, string $confirmCode): mixed;

    /**
     * Create reset token
     *
     * @param string $email
     * @param string $confirmCode
     * @return mixed
     */
    public function createResetToken(string $email, string $confirmCode): mixed;

    /**
     * Delete reset token
     *
     * @param string $email
     * @return mixed
     */
    public function deleteResetToken(string $email): mixed;

    /**
     * Find by confirm code
     *
     * @param $token
     * @return mixed
     */
    public function findByConfirmCode($token): mixed;
}
