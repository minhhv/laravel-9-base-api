<?php

namespace App\Repositories\Impl;

use App\Enums\Role;
use App\Enums\Status;
use App\Models\User;
use App\Repositories\BaseRepository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class UserRepositoryImpl extends BaseRepository implements UserRepositoryInterface
{
    public function getModel(): string
    {
        return User::class;
    }

    /**
     * Find by email
     *
     * @param $email
     * @return mixed
     */
    public function findByEmail($email): mixed
    {
        return $this->model
            ->with([
                'role' => static function ($query): void {
                    $query->select('roles.id', 'roles.name');
                },
            ])
            ->where('email', $email)
            ->first();
    }

    /**
     * Update user confirm code by email
     *
     * @param $email
     * @param $confirmCode
     * @return int|bool
     */
    public function updateUserConfirmCodeByEmail($email, $confirmCode): int|bool
    {
        return User::where('email', $email)->update(['confirm_code' => $confirmCode]);
    }

    /**
     * Store new user
     *
     * @param $userData
     * @return User
     */
    public function createUser($userData): User
    {
        return User::create([
            'user_name' => $userData['user_name'],
            'name' => $userData['user_name'],
            'email' => $userData['email'],
            'password' => Hash::make($userData['password']),
            'confirm_code' => $userData['confirm_code'],
        ]);
    }

    /**
     * Get roles
     *
     * @param $request
     * @return mixed
     */
    public function roles($request): mixed
    {
        $userId = $request->user()->id;

        return $this->model
            ->select([
                'id',
                'role_id',
            ])
            ->with([
                'role' => static function ($query): void {
                    $query->select('roles.id', 'roles.name');
                },
            ])
            ->where('id', $userId)
            ->get();
    }

    /**
     * Get permissions
     *
     * @param $request
     * @return mixed
     */
    public function getPermissions($request): mixed
    {
        $userId = $request->user()->id;

        return $this->model
            ->select([
                'id',
            ])
            ->with([
                'permissions' => static function ($query): void {
                    $query->select('permissions.id', 'permissions.name');
                },
            ])
            ->where('id', $userId)
            ->first();
    }

    /**
     * Get list user
     *
     * @param $data
     * @return mixed
     */
    public function users($data): mixed
    {
        return User::select(['id', 'email', 'name', 'user_name', 'password', 'is_locked'])
            ->orderBy('id', 'desc')
            ->paginate($data['per_page']);
    }

    /**
     * Get user with user role
     *
     * @param $id
     * @return mixed
     */
    public function getUserWithUserRole($id): mixed
    {
        return $this->model
            ->where('id', $id)
            ->where('role_id', Role::USER)
            ->first();
    }

    /**
     * Update password reset
     *
     * @param $email
     * @param $password
     * @return void
     */
    public function updatePasswordReset($email, $password): void
    {
        User::where('email', $email)
            ->first()
            ->update(['password' => Hash::make($password)]);
    }

    /**
     * Get profile
     *
     * @param $userId
     * @return mixed
     */
    public function getProfile($userId): mixed
    {
        return $this->model
            ->select(
                'users.*',
                'roles.name as role_name'
            )
            ->where([
                'users.id' => $userId,
            ])
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->first();
    }

    /**
     * Search users
     *
     * @param $request
     * @return mixed
     */
    public function search($request): mixed
    {
        $perPage = $request['per_page'] ?? config('const.per_page_user');
        $keyword = $request->keyword ?? null;

        return $this->model
            ->where('user_name', 'LIKE', "%{$keyword}%")
            ->orWhere('email', 'LIKE', "%{$keyword}%")
            ->with(['role'])
            ->paginate($perPage);
    }

    /**
     * Find valid user by id
     *
     * @param $id
     * @return mixed
     */
    public function findValidUserById($id): mixed
    {
        return $this->model
            ->where([
                'id' => $id,
                'is_active' => Status::ACTIVE,
                'is_locked' => Status::UNLOCK,
                'role_id' => Role::USER,
            ])
            ->first();
    }
}
