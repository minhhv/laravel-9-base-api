<?php

namespace App\Enums;

class Gender extends BaseEnum
{
    /**
     * 男・Male
     */
    public const MALE = 'MALE';

    /**
     * 女・Female
     */
    public const FEMALE = 'FEMALE';

    /**
     * その他・Other
     */
    public const OTHER = 'OTHER';
}
