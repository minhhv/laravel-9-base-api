<?php

namespace App\Enums;

class Role extends BaseEnum
{
    /**
     * Role User
     */
    public const USER = 1;

    /**
     * Role Admin
     */
    public const ADMIN = 2;

    /**
     * Role root admin
     */
    public const ROOT_ADMIN = 99;

    /**
     * Using change role for user
     */
    public const CHANGE_ROLE = [
        self::USER,
        self::ADMIN,
    ];

    /**
     * Role Guest.
     */
    public const GUEST = 3;
}
