<?php

namespace App\Enums;

class ErrorType extends BaseEnum
{
    /**
     * ERROR IP
     */
    public const ERROR_IP = 'ERROR_IP';

    /**
     * ERROR EMAIL
     */
    public const ERROR_EMAIL = 'ERROR_EMAIL';

    /**
     * ERROR PASSWORD
     */
    public const ERROR_PASSWORD = 'ERROR_PASSWORD';

    /**
     * ERROR USER IS LOCKED
     */
    public const ERROR_USER_LOCK = 'ERROR_USER_LOCK';
}
