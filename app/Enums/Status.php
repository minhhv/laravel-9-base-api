<?php

namespace App\Enums;

class Status extends BaseEnum
{
    /**
     * ACTIVE
     */
    public const ACTIVE = 'ACTIVE';

    /**
     * INACTIVE
     */
    public const INACTIVE = 'INACTIVE';

    /**
     * ALLOW
     */
    public const ALLOW = 'ALLOW';

    /**
     * NOT_ALLOW
     */
    public const NOT_ALLOW = 'NOT_ALLOW';

    /**
     * LOCK
     */
    public const LOCK = 'LOCK';

    /**
     * UNLOCK
     */
    public const UNLOCK = 'UNLOCK';

    /**
     * YES
     */
    public const YES = 'YES';

    /**
     * NO
     */
    public const NO = 'NO';

    /**
     * ENABLED
     */
    public const ENABLED = 'ENABLED';

    /**
     * DISABLED
     */
    public const DISABLED = 'DISABLED';
}
