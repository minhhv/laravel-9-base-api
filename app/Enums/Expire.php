<?php

namespace App\Enums;

class Expire extends BaseEnum
{
    /**
     * DAY_EXPIRE
     */
    public const DAY_EXPIRE = 14;
}
