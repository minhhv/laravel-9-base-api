<?php

namespace App\Enums;

class SiteConfig extends BaseEnum
{
    /**
     * Number of error when user login failed
     */
    public const ERROR_COUNT = 5;

    /**
     * Number of error reset when admin unlock user locked by system
     */
    public const ERROR_RESET = 0;
}
