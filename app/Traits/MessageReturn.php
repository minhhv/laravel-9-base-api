<?php

namespace App\Traits;

trait MessageReturn
{
    /**
     * Convert Array messages
     *
     * @param array $arrayMessage
     * @return array
     */
    public static function convertArrayMessage(array $arrayMessage): array
    {
        $errors = [];

        foreach ($arrayMessage as $val) {
            foreach ($val as $value) {
                $errors[] = $value;
            }
        }

        return array_unique($errors);
    }
}
