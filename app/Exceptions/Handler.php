<?php

namespace App\Exceptions;

use App\Services\Common\LogDebugService;
use App\Traits\APIResponse;
use App\Traits\MessageReturn;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use APIResponse;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (AuthenticationException $e) {
            LogDebugService::debugExceptionMessage($e);

            return $this->responseError([config('messages.ERROR.UNAUTHENTICATED')], Response::HTTP_UNAUTHORIZED);
        });

        $this->renderable(function (QueryException $e) {
            LogDebugService::debugExceptionMessage($e);

            return $this->responseError([config('messages.ERROR.QUERY')], Response::HTTP_BAD_REQUEST);
        });

        $this->renderable(function (NotFoundHttpException $e) {
            LogDebugService::debugExceptionMessage($e);

            return $this->responseError([config('messages.ERROR.NOT_FOUND')], Response::HTTP_NOT_FOUND);
        });

        $this->renderable(function (ModelNotFoundException $e) {
            LogDebugService::debugExceptionMessage($e);

            return $this->responseError([config('messages.ERROR.NOT_FOUND')], Response::HTTP_NOT_FOUND);
        });

        $this->renderable(function (ValidationException $e) {
            $errors = MessageReturn::convertArrayMessage($e->errors());

            return $this->responseError($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
        });

        $this->renderable(function (Throwable $e) {
            LogDebugService::debugExceptionMessage($e);

            return $this->responseError([
                config('messages.ERROR.PROCESS_FAILED'),
            ]);
        });
    }
}
