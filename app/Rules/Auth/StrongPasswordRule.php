<?php

namespace App\Rules\Auth;

use Illuminate\Contracts\Validation\Rule;

class StrongPasswordRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        // Must contain at least one lowercase letter, one uppercase letter, one digit, and one special character.
        return preg_match(
            '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/',
            $value
        );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('passwords.strong_error');
    }
}
