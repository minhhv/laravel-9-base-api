<?php

return [

    /*
    |--------------------------------------------------------------------------
    | バリデーションの言語行
    |--------------------------------------------------------------------------
    |
    | 次の言語行には、バリデータクラスで使用されるデフォルトの
    | エラーメッセージが含まれています。これらのルールの一部には、
    | サイズルールなどの複数のバージョンがあります。
    | ここでこれらの各メッセージを自由に調整してください。
    |
    */

    'accepted' => ':attributeを承認してください。',
    'active_url' => ':attributeには有効なURLを指定してください。',
    'after' => ':attributeには:date以降の日付を指定してください。',
    'after_or_equal' => ':attributeには:dateかそれ以降の日付を指定してください。',
    'alpha' => ':attributeには英字のみからなる文字列を指定してください。',
    'alpha_dash' => ':attributeには英数字・ハイフン・アンダースコアのみからなる文字列を指定してください。',
    'alpha_num' => ':attributeには英数字のみからなる文字列を指定してください。',
    'array' => ':attributeには配列を指定してください。',
    'before' => ':attributeには:date以前の日付を指定してください。',
    'before_or_equal' => ':attributeには:dateかそれ以前の日付を指定してください。',
    'between' => [
        'numeric' => ':attributeには:min〜:maxまでの数値を指定してください。',
        'file' => ':attributeには:min〜:max KBのファイルを指定してください。',
        'string' => ':attributeには:min〜:max文字の文字列を指定してください。',
        'array' => ':attributeには:min〜:max個の要素を持つ配列を指定してください。',
    ],
    'boolean' => ':attributeには真偽値を指定してください。',
    'confirmed' => ':attributeが確認用の値と一致しません。',
    'current_password' => '現在のパスワードが一致しません。',
    'date' => ':attributeは有効な日付ではありません。',
    'date_equals' => ':attributeは:dateと同じ日付でなければなりません。',
    'date_format' => ':attributeは:format形式と一致しません。',
    'different' => ':attributeには:otherとは異なる値を指定してください。',
    'digits' => ':attributeは:digits桁の数字でなければなりません。',
    'digits_between' => ':attributeは:min〜:max桁の数字である必要があります。',
    'dimensions' => ':attributeの画像サイズが無効です。',
    'distinct' => ':attributeに指定された値は重複しています。',
    'email' => ':attributeは有効なメールアドレスでなければなりません。',
    'ends_with' => ':attributeは、:valuesのいずれかで終了する必要があります。',
    'exists' => '指定された:attribute存在しません。',
    'file' => ':attributeはファイルでなければなりません。',
    'filled' => ':attributeには値が必要です。',
    'gt' => [
        'numeric' => ':attributeは:valueより大きくなければなりません。',
        'file' => ':attributeは:valueキロバイトより大きくなければなりません。',
        'string' => ':attributeは:value文字より大きくなければなりません。',
        'array' => ':attributeには:valueより多くのアイテムが必要です。',
    ],
    'gte' => [
        'numeric' => ':attributeは:value以上でなければなりません。',
        'file' => ':attributeは:valueキロバイト以上でなければなりません。',
        'string' => ':attributeは:value文字以上でなければなりません。',
        'array' => ':attributeには:value以上のアイテムが必要です。',
    ],
    'image' => ':attributeは画像でなければなりません。',
    'in' => '選択された:attributeは無効です。',
    'in_array' => ':attributeは:otherに存在しません。',
    'integer' => ':attributeは整数でなければなりません。',
    'ip' => ':attributeは有効なIPアドレスでなければなりません。',
    'ipv4' => ':attributeは有効なIPv4アドレスでなければなりません。',
    'ipv6' => ':attributeは有効なIPv6アドレスでなければなりません。',
    'json' => ':attributeは有効なJSON文字列でなければなりません。',
    'lt' => [
        'numeric' => ':attributeは:valueより小さくなければなりません。',
        'file' => ':attributeは:valueキロバイトより小さくなければなりません。',
        'string' => ':attributeは:value文字より小さくなければなりません。',
        'array' => ':attributeには:valueより少ないアイテムが必要です。',
    ],
    'lte' => [
        'numeric' => ':attributeは:value以下でなければなりません。',
        'file' => ':attributeは:valueキロバイト以下でなければなりません。',
        'string' => ':attributeは:value文字以下でなければなりません。',
        'array' => ':attributeには:value以下のアイテムが必要です。',
    ],
    'max' => [
        'numeric' => ':attributeは:maxより大きくてはいけません。',
        'file' => ':attributeは:maxキロバイトを超えてはいけません。',
        'string' => ':attributeは:max文字を超えてはいけません。',
        'array' => ':attributeには:max個を超えるアイテムを含めることはできません。',
    ],
    'mimes' => ':attributeは:valuesタイプのファイルでなければなりません。',
    'mimetypes' => ':attributeは:valuesタイプのファイルでなければなりません。',
    'mimetypes_csv' => ':attributeはcsvタイプのファイルでなければなりません。',
    'mimetypes_csv_xlsx' => ':attributeはcsv, xlsxタイプのファイルでなければなりません。',
    'min' => [
        'numeric' => ':attributeは:minより小さくてはいけません。',
        'file' => ':attributeは:minキロバイトより小さくてはいけません。',
        'string' => ':attributeは:min文字より小さくてはいけません。',
        'array' => ':attributeには少なくとも:min個のアイテムが必要です。',
    ],
    'multiple_of' => ':attributeは:valueの倍数である必要があります。',
    'not_in' => '選択された:attributeは無効です。',
    'not_regex' => __('message.const_validate.invalid_format_message'),
    'numeric' => ':attributeは数値でなければなりません。',
    'password' => 'パスワードが間違っています。',
    'present' => ':attributeが存在する必要があります。',
    'regex' => __('message.const_validate.invalid_format_message'),
    'required' => ':attributeは必須です。',
    'required_if' => ':otherが:valueの場合、:attributeは必須です。',
    'required_unless' => ':otherが:valueではない場合、:attributeは必須です。',
    'required_with' => ':valuesのうち一つでも存在する場合、:attributeは必須です。',
    'required_with_all' => ':valuesのうち全て存在する場合、:attributeは必須です。',
    'required_without' => ':valuesのうちどれか一つでも存在していない場合、:attributeは必須です。',
    'required_without_all' => ':valuesのうち全て存在していない場合、:attributeは必須です。',
    'prohibited' => ':attributeは禁止されています。',
    'prohibited_if' => ':otherが:valueの場合、:attributeは禁止されています。',
    'prohibited_unless' => ':otherが:valuesにない限り、:attributeは禁止されています。',
    'same' => ':attributeと:otherは一致する必要があります。',
    'size' => [
        'numeric' => ':attributeは:sizeでなければなりません。',
        'file' => ':attributeは:sizeキロバイトでなければなりません。',
        'string' => ':attributeは:size文字でなければなりません。',
        'array' => ':attributeには:sizeが含まれている必要があります。',
    ],
    'starts_with' => ':attributeは:valuesのいずれかで始まる必要があります。',
    'string' => ':attributeは文字列でなければなりません。',
    'timezone' => ':attributeは有効なタイムゾーンでなければなりません。',
    'unique' => ':attributeはすでに使用されています。',
    'uploaded' => ':attributeのアップロードに失敗しました。',
    'url' => ':attributeは有効なURLでなければなりません。',
    'uuid' => ':attributeは有効なUUIDでなければなりません。',
    'emoji_check' => ':attributeに絵文字は使用できません。',
    'invalid_char_input' => ':attributeに使用できない文字が含まれています。',
    'email_contain_forbidden_word' => 'あなたのメールアドレスには禁止されている言葉が含まれています。',
    'review_contain_forbidden_word' => 'レビューに禁止されている言葉が含まれています。',
    'account_not_exist' => 'アカウントが存在しません。',
    'group_not_exist' => '広告コードのグループIDが削除されました。',
    'check_mx_domain' => ':attributeに存在しないドメインが使用されています。正しいドメインを入力してください。',
    'image_max_size' => __('message.const_validate.attribute_prefix') .
        (int) config('const.image_repositories.max_size') / 1024 . 'Mbを超えてはいけません。',
    'check_number_int' => ':attributeには整数のみが含まれます。',
    'invalid_day' => '無効の日。',
    'email_blacklisted' => 'Email account is blacklisted.',
    'image_review_max_size' => __('message.const_validate.attribute_prefix') .
        (int) config('const.review.image_max_size') / 1024 . 'Mbを超えてはいけません。',
    'multiple_advertisement_code_empty' => 'カンマ（、）と一緒に値を入力してください。',
    'base_64_image_types' => __('message.const_validate.attribute_prefix') .
        config('const.review.image_types') . 'タイプのファイルでなければなりません。',
    'advertisement_code_not_exist' => '広告コードが存在しません。',
    'without_spaces' => 'スペースは許可されていません。',
    'no_special_characters' => __('message.const_validate.invalid_format_message'),
    'product_id_not_exist' => '商品IDが存在しません。',
    'invalid_site_mail_credentials' => ':attributeがサイトの資格情報と一致しません。',
    'date_after_now' => ':attributeに現在の日時以降の日付を指定してください。',
    /*
    |--------------------------------------------------------------------------
    | カスタムバリデーションの言語行
    |--------------------------------------------------------------------------
    |
    | ここでは、「attribute.rule」という規則を使用して行に名前を付けて、
    | 属性のカスタム検証メッセージを指定できます。 これにより、特定の属性ルールに
    | 特定のカスタム言語行をすばやく指定できます。
    |
    */

    'custom' => [
        'content' => [
            'max' => ':attributeは:max文字を超えてはいけません。',
        ],
        'content.*' => [
            'max' => __('message.const_validate.attribute_too_large'),
        ],
        'point' => [
            'between' => ':attributeには:min〜:maxまでの数値を指定してください。',
            'gte' => ':attributeには負の整数は使用できません',
        ],
        'display_message' => [
            'max' => __('message.const_validate.attribute_too_large'),
        ],
        'export_end_datetime' => [
            'date_format' => '日付と時刻無効な形式、形式：日-月-年時間：分期間',
        ],
        'export_start_datetime' => [
            'date_format' => '日付と時刻無効な形式、形式：日-月-年時間：分期間',
        ],
        'move_group_id' => [
            'required'  => '移行前の:attributeを選択してください。',
        ],
        'slt_group_id'  => [
            'required'  => '移行先の:attributeを選択してください。',
        ],
        'display_time' => [
            'between' => __('message.const_validate.attribute_too_large'),
            'gte' => ':attributeには負の整数は使用できません',
        ],
        'admin_product_id.*' => [
            'required_without' => '商品を選択してください。',
        ],
        'point_id' => [
            'required_without' => '商品を選択してください。',
            'required_without_product' => 'ポイントを選択してください。',
        ],
        'start_at.*' => [
            'date_format' => __('message.const_validate.invalid_date_time_format'),
        ],
        'end_at.*' => [
            'date_format' => __('message.const_validate.invalid_date_time_format'),
        ],
        'start_date.*' => [
            'date_format' => __('message.const_validate.invalid_date_time_format'),
        ],
        'end_date.*' => [
            'date_format' => __('message.const_validate.invalid_date_time_format'),
        ],
        'process_date_start' => [
            'date_format' => '対象期間は対象月を数字で指定してください。',
        ],
        'process_date_end' => [
            'date_format' => '対象期間は対象月を数字で指定してください。',
        ],
        'right-menu' => [
            'priority' => '入力した:attributeは無効です。',
        ],
        'thumbnail' => '無効なサムネイル。',
        'livestream' => [
            'delete_rule' => 'ライブを削除できません。',
            'update_rule' => 'ライブを更新できません。',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | カスタムバリデーション属性
    |--------------------------------------------------------------------------
    |
    | 次の言語行を使用して、属性プレースホルダーを「email」ではなく「E-Mail Address」などの
    | 読みやすいものに置き換えます。 これは単にメッセージをより表現力豊かにするのに役立ちます。
    |
    */

    'attributes' => [
        'email' => 'メールアドレス',
        'password' => 'パスワード',
        'agency' => '代理店',
        'login_id' => 'ログインID',
        'memo' => 'メモ',
        'current_password' => '現在のパスワード',
        'name' => '名前',
        'remarks' => '備考',
        'delete' => '削除',
        'is_active' => 'アクティブです',
        'point' => 'ポイント',
        'description' => '説明',
        'from_mail' => '送信元メール',
        'subject' => '件名',
        'body' => '内容',
        'is_login' => 'ログインです',
        'token' => 'アカウントトークン',
        'user_id' => 'ユーザーID',
        'status' => 'ステータス',
        'order_id' => '注文者ID',
        'year' => '年',
        'month' => '月',
        'day' => '日',
        'image' => '画像',
        'up_file' => 'アップロード',
        'code' => 'コード',
        'url' => 'URL',
        'detail' => '詳細',
        'ip_address' => 'IPアドレス',
        'is_allow' => '許可',
        'is_locked' => 'ロック',
        'management_department' => '管理部署',
        'administrator' => '管理者',
        'avatar' => 'アバター',
        'old_password' => '以前のパスワード',
        'new_password' => '新しいパスワードを入力',
        'new_password_confirmation' => 'パスワード再入力',
        'password_confirmation' => 'パスワード再入力',
        'user_name' => 'ユーザー名',
        'channel_id' => 'チャンネルID',
        'content' => 'コンテンツ',
        'reason' => '理由',
        'type' => 'タイプ',
        'tags' => 'タグ',
        'thumbnail' => 'サムネイル',
        'title' => 'タイトル',
        'video' => '動画',
        'filter' => 'フィルター',
        'message' => 'メッセージ',
        'livestream_ids' => 'ライブID',
        'title_action' => 'タイトル活動',
        'description_action' => '説明活動',
    ],
];
