# Laravel 9 base API

## Installation

### System Requirements
- PHP: ^8.1
- Mysql: ^8.0.*

### Configuration
Clone this repository

Copy and make the required configuration changes in the .env file: `cp .env.example .env`

Install dependencies with Npm
> `npm install`

Install dependencies with Composer
> `composer install`

Generate Key
> `php artisan key:generate`

Run the database migrations
> `php artisan migrate`

Run the database seeder
> `php artisan db:seed`

Create Key Passport
> `php artisan passport:install`

Clear Cache Config, Route, view
> `php artisan optimize:clear`

Clear autoload
> `composer dump-autoload`

Run test all API
> `php artisan test`

### Coding Standard
PSR2 

Check
> `composer run linter FILE_OR_FOLDER_PATH`

> Eg: `composer run linter app/Console`

Fix
> `composer run linter-fix FILE_OR_FOLDER_PATH`

> Eg: `composer run linter-fix app/Console`
