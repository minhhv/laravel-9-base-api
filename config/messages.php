<?php

return [
    'ERROR' => [
        'UNAUTHENTICATED' => 'UNAUTHENTICATED',
        'QUERY' => 'CANNOT_EXECUTE_QUERY',
        'NOT_FOUND' => 'RESOURCE_NOT_FOUND',
        'PROCESS_FAILED' => 'PROCESS_FAILED',
    ],
];
