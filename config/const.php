<?php

return [
    'config' => [
        'localhost_ip' => '127.0.0.1',
        'log_file_path' => 'logs/laravel.log',
    ],
    'route' => [
        'id_segment' => '/{id}',
    ],
    'password_length' => '8',
    'password_max_length' => '64',
    'user_name_min_length' => '1',
    'user_name_max_length' => '255',
    'name_min_length' => '4',
    'name_max_length' => '32',
    'per_page_search' => '10',
    'per_page_notify' => '10',
    'per_page_user' => '30',
    'per_page_livestream' => '10',
    'per_page_subscribed_channel' => '7',
    'per_page_my_videos' => '30',
    'per_page_library_videos' => '30',
    'per_page_comments_video' => '30',
    'per_page_children_comments_video' => '30',
    'per_page_list_playlists' => '10',
    'per_page_list_video_detail_page' => '10',
    'parent_id_default' => 0,
    'per_page_video_home' => 12,
    'POPULAR' => 'POPULAR',
    'RECENTLY' => 'RECENTLY',
    'per_page_tag' => 10,
    'email_length' => '8',
    'email_max_length' => '50',
    'content_max_length' => '65535',
    'common_device_property_max_length' => '32',
    'common_per_page' => '30',
    'per_page_admin_notification' => '30',
    'default_modify_id_notification' => 0,
    'category_name_max_length' => '255',
    'per_page_my_channel_list' => '30',
    'right_menu_max_length' => '100',
    'regex_email' => '/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i',
    'email_register_min_length' => '1',
    'email_register_max_length' => '255',
];
