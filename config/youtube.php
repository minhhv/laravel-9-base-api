<?php

return [
    'metrics' => [
        'views' => 'views',
        'estimatedMinutesWatched' => 'estimatedMinutesWatched',
        'averageViewDuration' => 'averageViewDuration',
    ],
];
